USE database;

CREATE TABLE user (
  userid   INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(255)     NOT NULL UNIQUE,
  hashed   CHAR(60),
  INDEX (username)
);

CREATE TABLE message (
  messageid   BIGINT UNSIGNED  NOT NULL AUTO_INCREMENT PRIMARY KEY,
  senderid    INTEGER UNSIGNED NOT NULL,
  recipientid INTEGER UNSIGNED NOT NULL,
  body        VARCHAR(2048),
  date_time   DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (senderid) REFERENCES user (userid),
  FOREIGN KEY (recipientid) REFERENCES user (userid),
  INDEX (senderid, recipientid)
);


CREATE TABLE image (
  imageid    INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  messageid  BIGINT UNSIGNED  NOT NULL,
  image_link VARCHAR(500)     NOT NULL,
  width      INTEGER          NOT NULL,
  height     INTEGER          NOT NULL,
  FOREIGN KEY (messageid) REFERENCES message (messageid),
  INDEX (messageid)
);

CREATE TABLE video (
  videoid      INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  messageid    BIGINT UNSIGNED  NOT NULL,
  video_link   VARCHAR(500)     NOT NULL,
  video_source VARCHAR(255)     NOT NULL,
  length_time  TIME             NOT NULL,
  FOREIGN KEY (messageid) REFERENCES message (messageid),
  INDEX (messageid)
);