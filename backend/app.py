import bcrypt
import flask
import pymysql
import pymysql.cursors
from flask import request, jsonify, make_response

app = flask.Flask(__name__)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not an endpoint'}), 404)


@app.errorhandler(500)
def not_found(error):
    return make_response(jsonify({'error': 'Internal server error'}), 500)


def connect_db():
    try:
        global db
        db = pymysql.connect(
            user='root',
            password='testpass',
            host='db',
            database='database',
        )
        return db
    except pymysql.err.DatabaseError:
        raise pymysql.err.DatabaseError


try:
    db = connect_db()
except pymysql.err.DatabaseError:
    db = None


def authenticate_user(username, password):
    if not username or not password:
        return False

    try:
        if not db:
            connect_db()
        with db.cursor() as cur:
            sql_get_user_hashed = "SELECT hashed FROM user WHERE username = %s;"
            cur.execute(sql_get_user_hashed, username)
            result = cur.fetchone()
            if not result:
                return False

            hashed = result[0].encode('utf-8')

            if bcrypt.checkpw(password.encode('utf-8'), hashed):
                return True
            else:
                return False
    except pymysql.err.Error:
        return make_response(jsonify({"error": "Database error"}), 503)


@app.route('/api/1.0/user', methods=['POST'])
def create_user():
    request_json = request.get_json()
    username = request_json.get('username')
    password = request_json.get('password')
    if not request_json or not username or not password:
        return make_response(jsonify({'error': 'Request must include username and password'}), 400)

    try:
        if not db:
            connect_db()
        with db.cursor() as cur:
            sql_find_user = "SELECT `username` FROM `user` WHERE `username` = %s;"
            cur.execute(sql_find_user, username)

            result = cur.fetchone()
            if result:
                return make_response(jsonify({'error': 'Username already exists'}), 400)

            hashed_password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
            decoded_hashed_password = hashed_password.decode('utf-8')

            sql_insert_user = "INSERT INTO user (username, hashed) VALUES (%s, %s);"
            cur.execute(sql_insert_user, (username, decoded_hashed_password))

            db.commit()
            return make_response(jsonify({'result': 'User created'}), 200)
    except pymysql.err.Error:
        return make_response(jsonify({"error": "Database error"}), 503)


@app.route('/api/1.0/messages', methods=['POST'])
def send_message():
    DEFAULT_WIDTH = 100
    DEFAULT_HEIGHT = 200
    DEFAULT_LENGTH = '00:05:00'
    DEFAULT_SOURCE = 'YouTube'

    if not request.authorization or not authenticate_user(
            request.authorization.get('username'), request.authorization.get('password')):
        return make_response(jsonify({'error': 'Authentication failed'}), 400)

    request_json = request.get_json()
    sender = request.authorization.get('username')
    recipient = request_json.get('recipient')
    body = request_json.get('body')
    attachment = request_json.get('attachment')

    if not request_json or not recipient or not (body or attachment):
        return make_response(jsonify(
            {'error': 'Request must include a recipient and a message body'}), 400)

    # Using fixed default values so no checking for attachment fields width, height, length_time
    if attachment and (not attachment.get('link') or not attachment.get('type')):
        return make_response(jsonify(
            {'error': 'Request attachment must include a link and type'}), 400)

    try:
        if not db:
            connect_db()
        with db.cursor() as cur:
            sql_select_userid = "SELECT userid FROM user WHERE username = %s"

            cur.execute(sql_select_userid, sender)
            senderid = cur.fetchone()
            if not senderid:
                return make_response(jsonify({'error': 'Sender does not exist'}), 404)

            cur.execute(sql_select_userid, recipient)
            recipientid = cur.fetchone()
            if not recipientid:
                return make_response(jsonify({'error': 'Recipient does not exist'}), 404)

            sql_insert_message = "INSERT INTO message (senderid, recipientid, body) VALUES (%s, %s, %s);"

            cur.execute(sql_insert_message, (senderid, recipientid, body))

            if attachment:
                link = attachment.get('link')
                message_type = attachment.get('type')

                sql_insert_image_link = """
                INSERT INTO image (messageid, image_link, width, height)
                VALUES (LAST_INSERT_ID(), %s, %s, %s)"""

                sql_insert_video_link = """
                INSERT INTO video (messageid, video_link, video_source, length_time)
                VALUES (LAST_INSERT_ID(), %s, %s, %s)
                """

                if message_type == 'image_link':
                    cur.execute(sql_insert_image_link, (link, DEFAULT_WIDTH, DEFAULT_HEIGHT))

                elif message_type == 'video_link':
                    cur.execute(sql_insert_video_link, (link, DEFAULT_SOURCE, DEFAULT_LENGTH))

            db.commit()

            return make_response(jsonify({'result': 'Message added'}), 200)

    except pymysql.err.Error:
        return make_response(jsonify({"error": "Database error"}), 503)


@app.route('/api/1.0/messages/', methods=['GET'])
def get_messages():
    user1 = request.args.get('user1')
    user2 = request.args.get('user2')
    messages_per_page = request.args.get('messages_per_page')
    page = request.args.get('page')

    if not (user1 and user2):
        return make_response(jsonify({'error': 'Parameters user1 and user2 are required'}), 400)
    if not request.authorization:
        return make_response(jsonify({'error': 'Authentication required'}), 400)
    login_user = request.authorization.get('username')
    if not (login_user == user1 or login_user == user2):
        return make_response(jsonify({'error': 'Authentication must be for one of the two users'}), 400)
    if not authenticate_user(login_user, request.authorization.get('password')):
        return make_response(jsonify({'error': 'Authentication failed'}), 400)
    if not messages_per_page and page:
        return make_response(jsonify({'error': 'Pagination requires messages_per_page'}), 400)
    if page is not None and int(page) < 1:
        return make_response(jsonify({'error': 'Invalid page number'}), 400)
    if messages_per_page is not None and int(messages_per_page) < 1:
        return make_response(jsonify({'error': 'Invalid messages_per_page number'}), 400)

    # Use large number to retrieve all rows
    # Quoted from MySQL documentation:
    # To retrieve all rows from a certain offset up to the end of the result set,
    # you can use some large number for the second parameter.
    limit = 18446744073709551615
    offset = 0
    if messages_per_page:
        limit = int(messages_per_page)
        if page:
            offset = limit * (int(page) - 1)

    try:
        if not db:
            connect_db()
        with db.cursor(pymysql.cursors.DictCursor) as cur:
            sql_get_messages = """
            SELECT 
                (SELECT username FROM user WHERE userid = senderid) AS sender,
                (SELECT username FROM user WHERE userid = recipientid) AS recipient, 
                body, date_time, 
                image.image_link AS image_link, image.width AS image_width, 
                image.height AS image_height, 
                video.video_link AS video_link, video.video_source AS video_source,
                video.length_time AS video_length_time                   
                FROM 
                    message
                LEFT JOIN image ON message.messageid = image.messageid
                LEFT JOIN video ON message.messageid = video.messageid
                WHERE 
                    (senderid = (SELECT userid FROM user WHERE username= %s)  AND 
                    recipientid = (SELECT userid FROM user WHERE username= %s)) OR 
                    (senderid = (SELECT userid FROM user WHERE username= %s) AND 
                    recipientid = (SELECT userid FROM user WHERE username= %s))
                ORDER BY date_time DESC
                LIMIT %s, %s;"""

            cur.execute(sql_get_messages, (user1, user2, user2, user1, offset, limit))

            results = cur.fetchall()

            cleaned_results = []
            for result in results:
                cleaned_results.append({k: str(v) for k, v in result.items() if v is not None})

            return make_response(jsonify({'result': cleaned_results}), 200)

    except pymysql.err.Error:
        return make_response(jsonify({"error": "Database error"}), 503)
