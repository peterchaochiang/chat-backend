# API

## Setup

    docker-compose up --build backend
    
The server will run on port 18000.

## REST API

Sample Postman requests are included.

### Authentication

HTTP requests to the REST API are protected with HTTP Basic authentication. 

### Create User

Takes a username and password and creates a new user in a persisted data store.
Make a json post request to /user/ and include a username and password field.

POST request
```/user

{
    "username": "example_user",
    "password": "example_password"
}
```

Upon success, a response with the result success will be sent along with the username with status code 200.

Response
```
{
    result: Success
}
```

There will be an error if a username or password is not included, or the username already exists.


### Post Message

Saves a message with a sender, recipient, and message. 

Three different message types are supported.
(1) is a basic text - only message.
(2) is an image link which contains a link, image width, and image height.
(3) is a video link which contains a link, video length, and source.


POST

/message

Request
```
Headers:    
    Basic Auth:
        username:
        password:

Basic Message
{
    "recipient": "example_recipient_user",
    "body": "example_message_body"
}

Image link
{
    "recipient": "example_recipient_user",
    "body": "",
    "attachment": {
        "type": "image_link",
        "link": "http://example.com/example.jpg",
        "width": 100,
        "height": 200
    }
}

Video link
{
    "recipient": "example_recipient_user",
    "body": "",
    "attachment": {
        "type": "video_link",
        "source": "YouTube",
        "video_length":"00:05:00"
    }
}
```

Upon success, a response with the result success will be sent along with the username with status code 200.

Response
```
{
    result: Success
}
```

There will be an error if auth fails or one of the required fields are missing.


### Fetch Message

* Fetch Messages

Takes two users and loads all messages sent between them. 
This call should also take two optional parameters in order to support pagination: 
the number of message to show per page and which page to load. 

Basic auth of one of the two users required.

Get Request
/message

```
Headers:
    Basic Auth:

parameters:
    user1
    user2
    page
    messages_per_page
```

Upon success, a response will be sent with the result containing the fetched messages.

Response
```
{
    result: [
        {
            "date_time": "2017-08-16 18:36:52",
            "recipient": "user2",
            "sender": "user1",
            "video_length_time": "0:05:00",
            "video_link": "jpg",
            "video_source": "YouTube"
        },
        {
            "body": "",
            "date_time": "2017-08-16 18:28:46",
            "recipient": "user2",
            "sender": "user1",
            "video_length_time": "0:05:00",
            "video_link": "jpg",
            "video_source": "YouTube"
        }
    ]
}
```

There will be an error if auth fails or one of the required fields are missing.

[![Analytics](https://ga-beacon.appspot.com/UA-128246049-1/chat-backend/README.md?pixel)](https://gitlab.com/peterchaochiang/badges/)

[![HitCount](http://hits.dwyl.io/peterchaochiang/test.svg)](http://hits.dwyl.io/peterchaochiang/test)
